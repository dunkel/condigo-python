#!/usr/bin/env python
"""
Class consulta a la base de datos 
:author: @Dunkel Kruspe 
:version: 2.0
"""
import MySQLdb
import string

class ibd:
	def __init__(self):
		'declarar variable'
		self.host		= 'localhost'
		self.user		= 'root'
		self.passwd		= 'admin'
		self.bd 		= 'sist_rad_tv'
		self.port		= 3306
		self.db 		= ''
		self.cursor 	= ''
	def ConectarDB(self):
		'Coneccion base de datos'
		self.db=MySQLdb.connect(self.host,self.user,self.passwd,self.bd,port=self.port)
		'resultado array asociado del query '
		self.cursor=self.db.cursor(MySQLdb.cursors.DictCursor)
		'sin array asociado'
#		self.cursor=self.db.cursor()
	def Liberar(self):
		if self.cursor:
			self.cursor.close()
	def Commit(self):
		self.db.commit()
	def Rollback(self):
		self.db.rollback()
class Consulta(ibd):
	def __init__(self,imprimir = None):
		ibd.__init__(self)
		self.total			= 0
		self.resultado 		= []
		self.Ultimoid 		= 0
		self.imprimir		= imprimir
	def Query(self,sql):
		ibd.ConectarDB(self)
		try:
   			self.cursor.execute(sql)
			self.Ultimoid = self.cursor.lastrowid
			self.Resultados()
			ibd.Commit(self)
		except:
			# Rollback in case there is any error
			self.total
			ibd.Rollback(self)
	def Resultados(self):
		self.total=self.cursor.rowcount
		#print self.total
		if self.total > 0:
			self.resultado=self.cursor.fetchall()
		else:
			self.total
		ibd.Liberar(self)
	def AgregarRegistro(self, tabla, nvoregistro):
		llaveDB = ""
		valorDB = ""
		for llave,valor in nvoregistro.iteritems():
			llaveDB+=llave+","
			valorDB+="'"+valor+"',"
		llaveDB=llaveDB[:-1]
		valorDB=valorDB[:-1]		
		sql = "INSERT INTO "+tabla+"("+llaveDB+") VALUES ("+valorDB+")"
		if not (self.imprimir is None):
			print sql;
		self.Query(sql)
	def ActualizarRegistro(self, tabla, nvoregistro, condicion):
		condicionesDB=""
		nvoregistroDB=""
		for llave,valor in nvoregistro.iteritems():
			nvoregistroDB+= llave+"='"+valor+"', "  		
		nvoregistroDB=nvoregistroDB[:-2]
		for llave,valor in condicion.iteritems():
			condicionesDB+=llave+"='"+valor+"' and "
		condicionesDB=condicionesDB[:-5]
		sql = "UPDATE "+tabla+" SET "+nvoregistroDB+" WHERE "+condicionesDB
		if not (self.imprimir is None):
			print sql;
		self.Query(sql)
	def ActualizarRegistroInner(self, tabla, nvoregistro, condicion):
		condicionesDB=""
		nvoregistroDB=""
		for llave,valor in nvoregistro.iteritems():
			nvoregistroDB+= llave+"='"+valor+"', "  		
		nvoregistroDB=nvoregistroDB[:-2]
		for llave,valor in condicion.iteritems():
			condicionesDB+=llave+"="+valor+" and "
		condicionesDB=condicionesDB[:-5]
		sql = "UPDATE "+tabla+" SET "+nvoregistroDB+" WHERE "+condicionesDB
		if not (self.imprimir is None):
			print sql;
		self.Query(sql)
	def ActualizarRegistroEspecial(self, tabla, nvoregistro, condicion):
		condicionesDB=""
		nvoregistroDB=""
		for llave,valor in nvoregistro.iteritems():
			nvoregistroDB+= llave+"="+valor+", "  		
		nvoregistroDB=nvoregistroDB[:-2]
		for llave,valor in condicion.iteritems():
			condicionesDB+=llave+"="+valor+" and "
		condicionesDB=condicionesDB[:-5]
		sql = "UPDATE "+tabla+" SET "+nvoregistroDB+" WHERE "+condicionesDB
		if not (self.imprimir is None):
			print sql;
		self.Query(sql)
	def EliminarRegistro(self, tabla, condicion):
		condicionesDB=""
		for llave,valor in condicion.iteritems():
			condicionesDB+=llave+"='"+valor+"' and "
		condicionesDB=condicionesDB[:-5]
		sql = "DELETE FROM "+tabla+" WHERE "+condicionesDB
		if not (self.imprimir is None):
			print sql;
		self.Query(sql)
	def SeleccionarTodo(self, tabla, campos, group, order):
		camposDB=""
		for valor in campos:
			camposDB+=valor+", "  		
		camposDB=camposDB[:-2]
		sql = "SELECT  "+camposDB+" FROM "+tabla
		if group: 
			sql+= " GROUP BY "+group
		if order: 
			sql+= " ORDER BY "+order
		if not (self.imprimir is None):
			print sql;
		self.Query(sql)
	def SeleccionarTablaFila(self, tabla, campos, condicion, group, order):
		camposDB=""
		condicionesDB=""
		for valor in campos:
			camposDB+=valor+", "  		
		camposDB=camposDB[:-2]
		for llave,valor in condicion.iteritems():
			condicionesDB+=llave+"="+valor+" and "
		condicionesDB=condicionesDB[:-5]
		sql = "SELECT  "+camposDB+" FROM "+tabla+" WHERE "+condicionesDB
		if group: 
			sql+= " GROUP BY "+group
		if order: 
			sql+= " ORDER BY "+order
		if not (self.imprimir is None):
			print sql;
		self.Query(sql)
	def QueryINNERJOIN(self, tabla, campos, condicion, group, order):
		camposDB=""
		condicionesDB=""
		for valor in campos:
			camposDB+=valor+", "  		
		camposDB=camposDB[:-2]
		for llave,valor in condicion.iteritems():
			condicionesDB+=llave+"="+valor+" and "
		condicionesDB=condicionesDB[:-5]
		sql = "SELECT  "+camposDB+" FROM "+tabla+" WHERE "+condicionesDB
		if group: 
			sql+= " GROUP BY "+group
		if order: 
			sql+= " ORDER BY "+order
		if not (self.imprimir is None):
			print sql;
		self.Query(sql)
	def Registros(self):
		return self.resultado
	def NumeroRegistros(self):
		return self.total
	def UltimoID(self):
		return self.Ultimoid
