#!/usr/bin/python
##############################################################
##Referencia 
##http://tecadmin.net/python-script-for-mysql-database-backup/
##
##copiar ssh 
##http://www.taringa.net/posts/linux/9635900/Combinacion-SSH-Cron-Rsync.html
##############################################################
import os
import time
import datetime

DB_HOST		= 'localhost'
DB_USER		= 'root'
DB_PASSWORD	= 'admin'
#DB_NAME	= '/backup/dbnames.txt'
DB_NAME		= 'dunkel'
BACKUP_PATH	= '/home/julio/respaldos/'
DATETIME = time.strftime('%m%d%Y-%H%M%S')
TODAYBACKUPPATH = BACKUP_PATH + DATETIME
if not os.path.exists(TODAYBACKUPPATH):
	os.makedirs(TODAYBACKUPPATH)
if os.path.exists(DB_NAME):
	file1 = open(DB_NAME)
	multi = 1
else:
	multi = 0

if multi:
	in_file = open(DB_NAME,"r")
	flength = len(in_file.readlines())
	in_file.close()
	p = 1
	dbfile = open(DB_NAME,"r")

	while p <= flength:
		db = dbfile.readline()	# reading database name from file
		db = db[:-1]		# deletes extra line
		dumpcmd = "mysqldump -u " + DB_USER + " -p" + DB_PASSWORD + " " + db + " > " + TODAYBACKUPPATH + "/" + db + ".sql"
		os.system(dumpcmd)
		p = p + 1
	dbfile.close()
else:
	db = DB_NAME
	dumpcmd = "mysqldump -u " + DB_USER + " -p" + DB_PASSWORD + " " + db + " > " + TODAYBACKUPPATH + "/" + db + ".sql"
	os.system(dumpcmd)

dumpcmd = "tar -czvf " + TODAYBACKUPPATH + ".tar.gz  " + TODAYBACKUPPATH + "/"
os.system(dumpcmd)
dumpcmd = "rm -r " + TODAYBACKUPPATH + "/"
os.system(dumpcmd)
