#!/usr/bin/env python
import string
import json
from datetime import *
from decimal import Decimal
from SOAPpy import WSDL         
url = 'http://127.0.0.1/d/server.php?wsdl'
namespace = 'urn:xmethods-MetodoPrueba'  

server = WSDL.Proxy('http://187.217.96.35/WsDeTesoreria/Service.asmx?WSDL') 
server.soapproxy.config.dumpSOAPOut=1
server.soapproxy.config.dumpSOAPIn=1
server.methods.keys()                                  
#[u'doGoogleSearch', u'doGetCachedPage', u'doSpellingSuggestion']
callInfo = server.methods['Recepcion_Pago_Infraccion_Tesoreria']
for arg in callInfo.inparams:
	print arg.name.ljust(15), arg.type
method={	'Usuario' :'SIIP_TESO',
		'Contrasena' :'S93VI022I35SP3432EP737783DT',
		'FolioInfraccion' :'NE33-42',
		'NombreContribuyente' : 'EDGAR MIGUEL',
		'PaternoContribuyente' : 'BRIONES',
		'MaternoContribuyente' : 'GUERRERO',
		'RfcContribuyente' : '-',
		'BanSexoContribuyente' : '1',
		'CalleContribuyente' : 'LAGO GUIJA',
		'ColoniaContribuyente' : 'AGUA AZUL',
		'CodigoPostalContribuyente' : '55700',
		'DelegacionMunicipioContribuyente' : 'Nezahualcoyotl',
		'FechaPago' : '03/06/2015 09:52:26',
		'NumeroReciboPago' : 'NZ 008756',
		'FormaPago' : 'EFECTIVO',
		'ImporteSubTotalPagado' : '4096.80',
		'ImporteDescuentoOtorgado' : '2048.40',
		'ImporteRecargosGenerados' : '0.00',
		'ImporteTotalPagado' : '2048.40',
		'Operacion' : 'PAGO',
		'MotivoCancelacion' : '-'
		}
results = server.Recepcion_Pago_Infraccion_Tesoreria(method);
print results
#The location of our webservice description
#WSDLFile = "http://127.0.0.1/d/server.php?wsdl"

#Object to handle the requests and responses from our
#webservice
#proxy = WSDL.Proxy(WSDLFile)
#proxy.soapproxy.config.dumpSOAPOut = 0
#proxy.soapproxy.config.dumpSOAPIn = 0
#method={'tcParametroA' :'SIIP_TESO','tcParametroB' :'S93VI022I35SP3432EP737783DT'}
#currencies = proxy.MetodoPrueba(method)
#print currencies
