#Copyright Jon Berg , turtlemeat.com

import string,cgi,time
from os import curdir, sep
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
from datetime import * 
from decimal import Decimal
from clases.consulta import *
#import pri

class MyHandler(BaseHTTPRequestHandler):

	def do_GET(self):
		try:
			if self.path=='/':
				u_path ='/index.html';
			else:
				u_path = self.path;
			f = open(curdir + sep + u_path) #self.path has /test.html
#note that this potentially makes every file on your computer readable by the internet

			self.send_response(200)
			self.send_header('Content-type',	'text/html')
			self.end_headers()
			campos=['*']
			mi_consulta= Consulta()
			mi_consulta.SeleccionarTodo('inverselogistics_devolucion',campos,'','item_id asc limit 1,100')
			total=mi_consulta.NumeroRegistros()
			self.wfile.write("<HTML><BODY>");
			if total>0:
				resultado=mi_consulta.Registros()
				i=0;
				for registro in resultado:
					#print (registro)
					self.wfile.write(""+str(i)+ '|');
					self.wfile.write(str(registro[0]) + '|' + str(registro[1]));
					self.wfile.write("<BR>");
					#print registro[0] , '|' , registro[1]
					i=1+i;
			else:
				#print "error";
				self.wfile.write("error<BR><BR>");
			self.wfile.write(f.read())
			self.wfile.write("</BODY></HTML>");
			f.close()	
			return
				
		except IOError:
			self.send_error(404,'File Not Found: %s' % self.path)
	 

	def do_POST(self,a):
		global rootnode
		try:
			self.send_response(200)
			self.send_header('Content-type',	'text/html')
			self.end_headers()
			self.wfile.write(a);
#			ctype, pdict = cgi.parse_header(self.headers.getheader('content-type'))
			
#			if ctype == 'multipart/form-data':
#				query=cgi.parse_multipart(self.rfile, pdict)
#			self.send_response(301)
#			
#			self.end_headers()
#			upfilecontent = query.get('upfile')
#			print "filecontent", upfilecontent[0]
#			self.wfile.write("<HTML>POST OK.<BR><BR>");
#			self.wfile.write(upfilecontent[0]);
			
		except :
			pass

def main():
	try:
		server = HTTPServer(('', 8000), MyHandler)
		print 'started httpserver...'
		server.serve_forever()
	except KeyboardInterrupt:
		print '^C received, shutting down server'
		server.socket.close()

if __name__ == '__main__':
	main()

