#!/usr/bin/env python
from suds.xsd.doctor import Import, ImportDoctor
from suds.client import Client

# enable logging to see transmitted XML
#import logging
#logging.basicConfig(level=logging.INFO)
#logging.getLogger('suds.client').setLevel(logging.DEBUG)

# fix broken wsdl
# add <s:import namespace="http://www.w3.org/2001/XMLSchema"/> to the wsdl
imp = Import('http://www.w3.org/2001/XMLSchema',
             location='http://www.w3.org/2001/XMLSchema.xsd')
imp.filter.add('http://tempuri.org/')
wsdl_url = 'http://187.217.96.35/WsDeTesoreria/Service.asmx?WSDL'
client = Client(wsdl_url, doctor=ImportDoctor(imp))
result = client.service.Recepcion_Pago_Infraccion_Tesoreria('SIIP_TESO','S93VI022I35SP3432EP737783DT','NE33-42','EDGAR MIGUEL','BRIONES','GUERRERO','-','1','LAGO GUIJA','AGUA AZUL','55700','Nezahualcoyotl','03/06/2015 09:52:26','NZ 008756','EFECTIVO','4096.80','2048.40','0.00','2048.40','PAGO','-');
print result
# make request
#arrayofstring = client.factory.create('parameters')
#print arrayofstring
#arrayofstring.string ={	'Usuario' :'SIIP_TESO',
#		'Contrasena' :'S93VI022I35SP3432EP737783DT',
#		'FolioInfraccion' :'NE33-42',
#		'NombreContribuyente' : 'EDGAR MIGUEL',
#		'PaternoContribuyente' : 'BRIONES',
#		'MaternoContribuyente' : 'GUERRERO',
#		'RfcContribuyente' : '-',
#		'BanSexoContribuyente' : '1',
#		'CalleContribuyente' : 'LAGO GUIJA',
#		'ColoniaContribuyente' : 'AGUA AZUL',
#		'CodigoPostalContribuyente' : '55700',
#		'DelegacionMunicipioContribuyente' : 'Nezahualcoyotl',
#		'FechaPago' : '03/06/2015 09:52:26',
#		'NumeroReciboPago' : 'NZ 008756',
#		'FormaPago' : 'EFECTIVO',
#		'ImporteSubTotalPagado' : '4096.80',
#		'ImporteDescuentoOtorgado' : '2048.40',
#		'ImporteRecargosGenerados' : '0.00',
#		'ImporteTotalPagado' : '2048.40',
#		'Operacion' : 'PAGO',
#		'MotivoCancelacion' : '-'
#		}
#print client.service.Recepcion_Pago_Infraccion_Tesoreria1D(arrayofstring).string
